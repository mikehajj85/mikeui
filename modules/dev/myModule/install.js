"use strict";
var uracModuleDevNav = [
	{
		'id': 'mike-module',
		'label': "Mike Custom Module",
		'url': '#/mike-module',
		'tplPath': 'modules/dev/myModule/directives/listTenants.tmpl',
		'icon': 'users',
		'checkPermission': {
			'service': 'dashboard',
			'route': '/tenant/list',
			'method': 'get'
		},
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'contentMenu': true,
		'tracker': true,
		'order': 100,
		'scripts': ['modules/dev/myModule/config.js', 'modules/dev/myModule/controller.js'],
		'ancestor': [translation.home[LANG]]
	}
];

navigation = navigation.concat(uracModuleDevNav);