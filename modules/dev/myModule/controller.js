"use strict";

var mikeApp = soajsApp.components;
mikeApp.controller("mikeCtrl", ['$scope', 'ngDataApi', function ($scope, ngDataApi) {
	$scope.$parent.isUserLoggedIn();
	$scope.access = {};
	$scope.selectedEnv = $scope.$parent.currentSelectedEnvironment.toUpperCase();
	var permissions = {
		"listTenants": ['dashboard', '/tenant/list', 'get']
	};
	constructModulePermissions($scope, $scope.access, permissions);

	$scope.listTenants = function () {
		overlayLoading.show();
		var opts = {
			"routeName": "/dashboard/tenant/list",
			"method": "get",
			"params": {
				"type": "admin",
				"negate": true
			}
		};
		getSendDataFromServer($scope, ngDataApi, opts, function (error, response) {
			overlayLoading.hide();
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'dashboard', error.message);
			}
			else {
				for (var x = response.length - 1; x >= 0; x--) {
					if (response[x].type === 'admin') {
						response.splice(x, 1);
					}
				}
				$scope.tenants = response;
			}
		});

	};

	if ($scope.access.listTenants) {
		$scope.listTenants();
	}
}]);
